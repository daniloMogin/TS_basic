var zadatak4;
(function (zadatak4) {
    var Menu = /** @class */ (function () {
        function Menu(item_list, total_pages) {
            this.items = item_list;
            this.pages = total_pages;
        }
        Menu.prototype.list = function () {
            console.log("***/**********************/***");
            console.log("Naš dnevni meni:");
            for (var i = 0, item_len = this.items.length; i < item_len; i++) {
                console.log(i + 1 + ': ' + this.items[i]);
            }
            console.log("***/**********************/***");
        };
        return Menu;
    }());
    var sundayMenu = new Menu(["pljeskavica", "burek", "gibanica", "pogačica sa čvarcima"], 1);
    sundayMenu.list();
})(zadatak4 || (zadatak4 = {}));
