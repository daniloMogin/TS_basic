var gulp = require('gulp'),
    ts = require('gulp-typescript');

gulp.task('zadatak1', function() {
    return gulp.src('zadatak1/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            outFile: 'output.js'
        }))
        .pipe(gulp.dest('zadatak1'));
});
gulp.task('watch-zadatak1', function() {
    gulp.watch('zadatak1/**/*.ts', ['zadatak1']);
});

gulp.task('zadatak2', function() {
    return gulp.src('zadatak2/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            outFile: 'output.js'
        }))
        .pipe(gulp.dest('zadatak2'));
});
gulp.task('watch-zadatak2', function() {
    gulp.watch('zadatak2/**/*.ts', ['zadatak2']);
});

gulp.task('zadatak3', function() {
    return gulp.src('zadatak3/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            outFile: 'output.js'
        }))
        .pipe(gulp.dest('zadatak3'));
});
gulp.task('watch-zadatak3', function() {
    gulp.watch('zadatak3/**/*.ts', ['zadatak3']);
});

gulp.task('zadatak4', function() {
    return gulp.src('zadatak4/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            outFile: 'output.js'
        }))
        .pipe(gulp.dest('zadatak4'));
});
gulp.task('watch-zadatak4', function() {
    gulp.watch('zadatak4/**/*.ts', ['zadatak4']);
});

gulp.task('zadatak5', function() {
    return gulp.src('zadatak5/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            outFile: 'output.js'
        }))
        .pipe(gulp.dest('zadatak5'));
});
gulp.task('watch-zadatak5', function() {
    gulp.watch('zadatak5/**/*.ts', ['zadatak5']);
});

gulp.task('default', [
    'watch-zadatak1',
    'watch-zadatak2',
    'watch-zadatak3',
    'watch-zadatak4',
    'watch-zadatak5'
], function() {})