namespace zadatak5 {
    class Menu {
        public items: Array<string>; // niz stringova
        public pages: number;

        public constructor(item_list: Array<string>, total_pages: number) {
            this.items = item_list;
            this.pages = total_pages;
        }

        public list(): void {
            console.log(`***/**********************/***`);
            console.log("Naš dnevni meni:");
            for (let i: number = 0, item_len: number = this.items.length; i < item_len; i++) {
                console.log(i + 1 + ': ' + this.items[i]);
            }
            console.log(`***/**********************/***`);
        }

    }

    const sundayMenu = new Menu(["pljeskavica", "burek", "gibanica", "pogačica sa čvarcima"], 1);

    sundayMenu.list();


    class SpecialMenu extends Menu {
        constructor(item_list: Array<string>, total_pages: number) {
            super(item_list, total_pages);
        }

        list(): void {
            console.log(`***/**********************/***`);
            console.log("Naši specijali:");
            for (let i: number = 0, item_len: number = this.items.length; i < item_len; i++) {
                console.log(i + 1 + ': ' + this.items[i]);
            }
            console.log(`***/**********************/***`);
        }
    }

    const today_special = new SpecialMenu(["Burek sa mesom i jogurt", "Pasulj sa kolenicom i mekikama", "Skembici sa hlebom"], 1);

    today_special.list();
}