var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var zadatak5;
(function (zadatak5) {
    var Menu = /** @class */ (function () {
        function Menu(item_list, total_pages) {
            this.items = item_list;
            this.pages = total_pages;
        }
        Menu.prototype.list = function () {
            console.log("***/**********************/***");
            console.log("Naš dnevni meni:");
            for (var i = 0, item_len = this.items.length; i < item_len; i++) {
                console.log(i + 1 + ': ' + this.items[i]);
            }
            console.log("***/**********************/***");
        };
        return Menu;
    }());
    var sundayMenu = new Menu(["pljeskavica", "burek", "gibanica", "pogačica sa čvarcima"], 1);
    sundayMenu.list();
    var SpecialMenu = /** @class */ (function (_super) {
        __extends(SpecialMenu, _super);
        function SpecialMenu(item_list, total_pages) {
            return _super.call(this, item_list, total_pages) || this;
        }
        SpecialMenu.prototype.list = function () {
            console.log("***/**********************/***");
            console.log("Naši specijali:");
            for (var i = 0, item_len = this.items.length; i < item_len; i++) {
                console.log(i + 1 + ': ' + this.items[i]);
            }
            console.log("***/**********************/***");
        };
        return SpecialMenu;
    }(Menu));
    var today_special = new SpecialMenu(["Burek sa mesom i jogurt", "Pasulj sa kolenicom i mekikama", "Skembici sa hlebom"], 1);
    today_special.list();
})(zadatak5 || (zadatak5 = {}));
