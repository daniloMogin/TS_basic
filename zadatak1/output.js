var zadatak1;
(function (zadatak1) {
    var burger = 'pljeskavica', calories = 800, tasty = true;
    function speak(food, energy, tasty) {
        console.log("***/**********************/***");
        if (tasty) {
            console.log("Naša " + food + " ima " + energy + " kalorija. ");
        }
        else {
            console.log("Na\u0161a " + food + " vam nije bila ukusna :(");
        }
        console.log("***/**********************/***");
    }
    speak(burger, calories, tasty);
})(zadatak1 || (zadatak1 = {}));
