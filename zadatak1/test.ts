namespace zadatak1 {
    let burger: string = 'pljeskavica',
        calories: number = 800,
        tasty: boolean = true;

    function speak(food: string, energy: number, tasty: boolean): void {
        console.log(`***/**********************/***`);
        if (tasty) {
            console.log("Naša " + food + " ima " + energy + " kalorija. ");
        } else {
            console.log(`Naša ${food} vam nije bila ukusna :(`);
        }
        console.log(`***/**********************/***`);
    }
    speak(burger, calories, tasty);
}
