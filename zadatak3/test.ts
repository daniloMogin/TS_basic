namespace zadatak3 {
    // Ovako se definise interface
    interface Food {
        name: string;
        calories: number;
    }
    // Ovako kazemo funkciji da ocekuje objekat tipa "Food" interface
    function speak(food: Food): void {
        console.log(`***/**********************/***`);
        console.log("Naš " + food.name + " ima " + food.calories + " kalorija.");
        console.log(`***/**********************/***`);
    }

    const ice_cream = {
        name: "sladoled",
        calories: 200
    }

    speak(ice_cream);
} 