var zadatak3;
(function (zadatak3) {
    // Ovako kazemo funkciji da ocekuje objekat tipa "Food" interface
    function speak(food) {
        console.log("***/**********************/***");
        console.log("Naš " + food.name + " ima " + food.calories + " kalorija.");
        console.log("***/**********************/***");
    }
    var ice_cream = {
        name: "sladoled",
        calories: 200
    };
    speak(ice_cream);
})(zadatak3 || (zadatak3 = {}));
