namespace zadatak2 {
    const array1 = [1, 2, 3];
    const array2 = [4, 5, 6, 7];

    function concatArray(...arrays: any[]) {
        console.log(`***/**********************/***`);
        console.log(arrays);
    }

    // varijanta 1
    concatArray(...array1, ...array2);
    // varijanta 2
    concatArray(...array2, ...array1);

    const obj1 = {
        name: 'Test',
        age: '2',
    }
    const obj2 = {
        ...obj1,
        email: 'test@test.com'
    }
    const obj3 = {
        ...obj2,
        email: 'izmena@test.com'
    }

    console.log(`***/**********************/***`);
    console.log(`obj1`);
    console.log(obj1);
    console.log(`***/**********************/***`);
    console.log(`obj2`);
    console.log(obj2);
    console.log(`***/**********************/***`);
    console.log(`obj3`);
    console.log(obj3);
    console.log(`***/**********************/***`);
} 