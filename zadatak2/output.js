var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var zadatak2;
(function (zadatak2) {
    var array1 = [1, 2, 3];
    var array2 = [4, 5, 6, 7];
    function concatArray() {
        var arrays = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arrays[_i] = arguments[_i];
        }
        console.log("***/**********************/***");
        console.log(arrays);
    }
    // varijanta 1
    concatArray.apply(void 0, array1.concat(array2));
    // varijanta 2
    concatArray.apply(void 0, array2.concat(array1));
    var obj1 = {
        name: 'Test',
        age: '2'
    };
    var obj2 = __assign({}, obj1, { email: 'test@test.com' });
    var obj3 = __assign({}, obj2, { email: 'izmena@test.com' });
    console.log("***/**********************/***");
    console.log("obj1");
    console.log(obj1);
    console.log("***/**********************/***");
    console.log("obj2");
    console.log(obj2);
    console.log("***/**********************/***");
    console.log("obj3");
    console.log(obj3);
    console.log("***/**********************/***");
})(zadatak2 || (zadatak2 = {}));
